@rem
@rem Copyright 2015 the original author or authors.
@rem
@rem Licensed under the Apache License, Version 2.0 (the "License");
@rem you may not use this file except in compliance with the License.
@rem You may obtain a copy of the License at
@rem
@rem      https://www.apache.org/licenses/LICENSE-2.0
@rem
@rem Unless required by applicable law or agreed to in writing, software
@rem distributed under the License is distributed on an "AS IS" BASIS,
@rem WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@rem See the License for the specific language governing permissions and
@rem limitations under the License.
@rem

@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  masoffer-traffic-buff startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Resolve any "." and ".." in APP_HOME to make it shorter.
for %%i in ("%APP_HOME%") do set APP_HOME=%%~fi

@rem Add default JVM options here. You can also use JAVA_OPTS and MASOFFER_TRAFFIC_BUFF_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS="-Duser.dir=%~dp0.." "-Dvertx.cwd=%~dp0.." "-Dlogback.configurationFile=%~dp0../conf/logback.groovy" "-Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory" "-Xmx512m"

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto execute

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto execute

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\masoffer-traffic-buff-21.06.26.0-SNAPSHOT.jar;%APP_HOME%\lib\gpars-1.2.1.jar;%APP_HOME%\lib\logback-classic-1.2.1.jar;%APP_HOME%\lib\logback-core-1.2.1.jar;%APP_HOME%\lib\unirest-java-3.11.09.jar;%APP_HOME%\lib\jbrowserdriver-1.1.1.jar;%APP_HOME%\lib\zt-process-killer-1.10.jar;%APP_HOME%\lib\commons-lang3-3.8.1.jar;%APP_HOME%\lib\geb-core-4.1.jar;%APP_HOME%\lib\selenium-java-3.141.59.jar;%APP_HOME%\lib\selenium-support-3.141.59.jar;%APP_HOME%\lib\mongodb-driver-async-3.4.3.jar;%APP_HOME%\lib\vertx-web-3.4.1.jar;%APP_HOME%\lib\vertx-auth-shiro-3.4.1.jar;%APP_HOME%\lib\vertx-auth-common-3.4.1.jar;%APP_HOME%\lib\jackson-dataformat-cbor-2.8.7.jar;%APP_HOME%\lib\jackson-dataformat-smile-2.8.7.jar;%APP_HOME%\lib\redisson-3.13.6.jar;%APP_HOME%\lib\vertx-core-3.4.1.jar;%APP_HOME%\lib\jackson-dataformat-yaml-2.11.1.jar;%APP_HOME%\lib\jackson-databind-2.11.1.jar;%APP_HOME%\lib\jackson-core-2.11.1.jar;%APP_HOME%\lib\jackson-annotations-2.11.1.jar;%APP_HOME%\lib\failsafe-1.0.4.jar;%APP_HOME%\lib\completable-futures-0.3.0.jar;%APP_HOME%\lib\fastjson-1.2.7.jar;%APP_HOME%\lib\selenium-firefox-driver-3.141.59.jar;%APP_HOME%\lib\selenium-chrome-driver-3.141.59.jar;%APP_HOME%\lib\selenium-edge-driver-3.141.59.jar;%APP_HOME%\lib\selenium-ie-driver-3.141.59.jar;%APP_HOME%\lib\selenium-opera-driver-3.141.59.jar;%APP_HOME%\lib\selenium-safari-driver-3.141.59.jar;%APP_HOME%\lib\selenium-remote-driver-4.0.0-alpha-2.jar;%APP_HOME%\lib\guava-30.1.1-jre.jar;%APP_HOME%\lib\groovy-ant-2.5.4.jar;%APP_HOME%\lib\groovy-cli-commons-2.5.4.jar;%APP_HOME%\lib\groovy-groovysh-2.5.4.jar;%APP_HOME%\lib\groovy-console-2.5.4.jar;%APP_HOME%\lib\groovy-groovydoc-2.5.4.jar;%APP_HOME%\lib\groovy-docgenerator-2.5.4.jar;%APP_HOME%\lib\groovy-cli-picocli-2.5.4.jar;%APP_HOME%\lib\groovy-datetime-2.5.4.jar;%APP_HOME%\lib\groovy-jmx-2.5.4.jar;%APP_HOME%\lib\groovy-json-2.5.4.jar;%APP_HOME%\lib\groovy-jsr223-2.5.4.jar;%APP_HOME%\lib\groovy-nio-2.5.4.jar;%APP_HOME%\lib\groovy-servlet-2.5.4.jar;%APP_HOME%\lib\groovy-sql-2.5.4.jar;%APP_HOME%\lib\groovy-swing-2.5.4.jar;%APP_HOME%\lib\groovy-test-2.5.4.jar;%APP_HOME%\lib\groovy-test-junit5-2.5.4.jar;%APP_HOME%\lib\groovy-testng-2.5.4.jar;%APP_HOME%\lib\geb-ast-4.1.jar;%APP_HOME%\lib\geb-waiting-4.1.jar;%APP_HOME%\lib\geb-implicit-assertions-4.1.jar;%APP_HOME%\lib\geb-exceptions-4.1.jar;%APP_HOME%\lib\groovy-templates-2.5.14.jar;%APP_HOME%\lib\groovy-xml-2.5.14.jar;%APP_HOME%\lib\groovy-macro-2.5.14.jar;%APP_HOME%\lib\groovy-2.5.14.jar;%APP_HOME%\lib\multiverse-core-0.7.0.jar;%APP_HOME%\lib\jsr166y-1.7.0.jar;%APP_HOME%\lib\jodd-lagarto-5.1.5.jar;%APP_HOME%\lib\jodd-log-5.1.5.jar;%APP_HOME%\lib\zt-exec-1.7.jar;%APP_HOME%\lib\slf4j-api-1.7.30.jar;%APP_HOME%\lib\httpmime-4.5.13.jar;%APP_HOME%\lib\httpclient-cache-4.5.10.jar;%APP_HOME%\lib\httpclient-4.5.13.jar;%APP_HOME%\lib\httpcore-nio-4.4.13.jar;%APP_HOME%\lib\httpasyncclient-4.1.4.jar;%APP_HOME%\lib\gson-2.8.6.jar;%APP_HOME%\lib\selenium-api-4.0.0-alpha-2.jar;%APP_HOME%\lib\threeten-extra-1.4.jar;%APP_HOME%\lib\byte-buddy-1.10.14.jar;%APP_HOME%\lib\commons-exec-1.3.jar;%APP_HOME%\lib\okhttp-3.14.0.jar;%APP_HOME%\lib\okio-1.17.2.jar;%APP_HOME%\lib\mongodb-driver-core-3.4.3.jar;%APP_HOME%\lib\bson-3.4.3.jar;%APP_HOME%\lib\shiro-core-1.2.4.jar;%APP_HOME%\lib\snakeyaml-1.27.jar;%APP_HOME%\lib\netty-resolver-dns-4.1.52.Final.jar;%APP_HOME%\lib\netty-codec-http2-4.1.8.Final.jar;%APP_HOME%\lib\netty-handler-4.1.52.Final.jar;%APP_HOME%\lib\netty-handler-proxy-4.1.8.Final.jar;%APP_HOME%\lib\netty-codec-http-4.1.8.Final.jar;%APP_HOME%\lib\netty-codec-dns-4.1.52.Final.jar;%APP_HOME%\lib\netty-codec-socks-4.1.8.Final.jar;%APP_HOME%\lib\netty-codec-4.1.52.Final.jar;%APP_HOME%\lib\netty-transport-4.1.52.Final.jar;%APP_HOME%\lib\netty-buffer-4.1.52.Final.jar;%APP_HOME%\lib\netty-resolver-4.1.52.Final.jar;%APP_HOME%\lib\netty-common-4.1.52.Final.jar;%APP_HOME%\lib\cache-api-1.0.0.jar;%APP_HOME%\lib\reactor-core-3.3.9.RELEASE.jar;%APP_HOME%\lib\rxjava-2.2.19.jar;%APP_HOME%\lib\jboss-marshalling-river-2.0.10.Final.jar;%APP_HOME%\lib\jodd-bean-5.1.6.jar;%APP_HOME%\lib\failureaccess-1.0.1.jar;%APP_HOME%\lib\listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar;%APP_HOME%\lib\jsr305-3.0.2.jar;%APP_HOME%\lib\checker-qual-3.8.0.jar;%APP_HOME%\lib\error_prone_annotations-2.5.1.jar;%APP_HOME%\lib\j2objc-annotations-1.3.jar;%APP_HOME%\lib\selenium-server-4.0.0-alpha-2.jar;%APP_HOME%\lib\classgraph-4.8.47.jar;%APP_HOME%\lib\commons-lang-2.6.jar;%APP_HOME%\lib\ant-junit-1.9.13.jar;%APP_HOME%\lib\ant-1.9.13.jar;%APP_HOME%\lib\ant-launcher-1.9.13.jar;%APP_HOME%\lib\ant-antlr-1.9.13.jar;%APP_HOME%\lib\commons-cli-1.4.jar;%APP_HOME%\lib\picocli-3.7.0.jar;%APP_HOME%\lib\qdox-1.12.1.jar;%APP_HOME%\lib\jline-2.14.6.jar;%APP_HOME%\lib\junit-4.12.jar;%APP_HOME%\lib\junit-platform-launcher-1.3.1.jar;%APP_HOME%\lib\junit-jupiter-engine-5.3.1.jar;%APP_HOME%\lib\testng-6.13.1.jar;%APP_HOME%\lib\httpcore-4.4.13.jar;%APP_HOME%\lib\commons-logging-1.2.jar;%APP_HOME%\lib\commons-codec-1.11.jar;%APP_HOME%\lib\jodd-core-5.1.6.jar;%APP_HOME%\lib\commons-beanutils-1.8.3.jar;%APP_HOME%\lib\reactive-streams-1.0.3.jar;%APP_HOME%\lib\jboss-marshalling-2.0.10.Final.jar;%APP_HOME%\lib\opentracing-tracerresolver-0.1.7.jar;%APP_HOME%\lib\opentracing-util-0.33.0.jar;%APP_HOME%\lib\jna-4.2.2.jar;%APP_HOME%\lib\commons-io-2.2.jar;%APP_HOME%\lib\hamcrest-core-1.3.jar;%APP_HOME%\lib\junit-platform-engine-1.3.1.jar;%APP_HOME%\lib\junit-jupiter-api-5.3.1.jar;%APP_HOME%\lib\junit-platform-commons-1.3.1.jar;%APP_HOME%\lib\apiguardian-api-1.0.0.jar;%APP_HOME%\lib\jcommander-1.72.jar;%APP_HOME%\lib\log4j-api-2.8.2.jar;%APP_HOME%\lib\opentracing-noop-0.33.0.jar;%APP_HOME%\lib\opentracing-api-0.33.0.jar;%APP_HOME%\lib\opentest4j-1.1.1.jar


@rem Execute masoffer-traffic-buff
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %MASOFFER_TRAFFIC_BUFF_OPTS%  -classpath "%CLASSPATH%" app.Runner %*

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable MASOFFER_TRAFFIC_BUFF_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%MASOFFER_TRAFFIC_BUFF_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
