#!/usr/bin/env sh

#
# Copyright 2015 the original author or authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

##############################################################################
##
##  masoffer-traffic-buff start up script for UN*X
##
##############################################################################

# Attempt to set APP_HOME
# Resolve links: $0 may be a link
PRG="$0"
# Need this for relative symlinks.
while [ -h "$PRG" ] ; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
        PRG="$link"
    else
        PRG=`dirname "$PRG"`"/$link"
    fi
done
SAVED="`pwd`"
cd "`dirname \"$PRG\"`/.." >/dev/null
APP_HOME="`pwd -P`"
cd "$SAVED" >/dev/null

APP_NAME="masoffer-traffic-buff"
APP_BASE_NAME=`basename "$0"`

# Add default JVM options here. You can also use JAVA_OPTS and MASOFFER_TRAFFIC_BUFF_OPTS to pass JVM options to this script.
DEFAULT_JVM_OPTS='"-Duser.dir=$APP_HOME" "-Dvertx.cwd=$APP_HOME" "-Dlogback.configurationFile=$APP_HOME/conf/logback.groovy" "-Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory" "-Xmx512m"'

# Use the maximum available, or set MAX_FD != -1 to use that value.
MAX_FD="maximum"

warn () {
    echo "$*"
}

die () {
    echo
    echo "$*"
    echo
    exit 1
}

# OS specific support (must be 'true' or 'false').
cygwin=false
msys=false
darwin=false
nonstop=false
case "`uname`" in
  CYGWIN* )
    cygwin=true
    ;;
  Darwin* )
    darwin=true
    ;;
  MINGW* )
    msys=true
    ;;
  NONSTOP* )
    nonstop=true
    ;;
esac

CLASSPATH=$APP_HOME/lib/masoffer-traffic-buff-21.06.26.0-SNAPSHOT.jar:$APP_HOME/lib/gpars-1.2.1.jar:$APP_HOME/lib/logback-classic-1.2.1.jar:$APP_HOME/lib/logback-core-1.2.1.jar:$APP_HOME/lib/unirest-java-3.11.09.jar:$APP_HOME/lib/jbrowserdriver-1.1.1.jar:$APP_HOME/lib/zt-process-killer-1.10.jar:$APP_HOME/lib/commons-lang3-3.8.1.jar:$APP_HOME/lib/geb-core-4.1.jar:$APP_HOME/lib/selenium-java-3.141.59.jar:$APP_HOME/lib/selenium-support-3.141.59.jar:$APP_HOME/lib/mongodb-driver-async-3.4.3.jar:$APP_HOME/lib/vertx-web-3.4.1.jar:$APP_HOME/lib/vertx-auth-shiro-3.4.1.jar:$APP_HOME/lib/vertx-auth-common-3.4.1.jar:$APP_HOME/lib/jackson-dataformat-cbor-2.8.7.jar:$APP_HOME/lib/jackson-dataformat-smile-2.8.7.jar:$APP_HOME/lib/redisson-3.13.6.jar:$APP_HOME/lib/vertx-core-3.4.1.jar:$APP_HOME/lib/jackson-dataformat-yaml-2.11.1.jar:$APP_HOME/lib/jackson-databind-2.11.1.jar:$APP_HOME/lib/jackson-core-2.11.1.jar:$APP_HOME/lib/jackson-annotations-2.11.1.jar:$APP_HOME/lib/failsafe-1.0.4.jar:$APP_HOME/lib/completable-futures-0.3.0.jar:$APP_HOME/lib/fastjson-1.2.7.jar:$APP_HOME/lib/selenium-firefox-driver-3.141.59.jar:$APP_HOME/lib/selenium-chrome-driver-3.141.59.jar:$APP_HOME/lib/selenium-edge-driver-3.141.59.jar:$APP_HOME/lib/selenium-ie-driver-3.141.59.jar:$APP_HOME/lib/selenium-opera-driver-3.141.59.jar:$APP_HOME/lib/selenium-safari-driver-3.141.59.jar:$APP_HOME/lib/selenium-remote-driver-4.0.0-alpha-2.jar:$APP_HOME/lib/guava-30.1.1-jre.jar:$APP_HOME/lib/groovy-ant-2.5.4.jar:$APP_HOME/lib/groovy-cli-commons-2.5.4.jar:$APP_HOME/lib/groovy-groovysh-2.5.4.jar:$APP_HOME/lib/groovy-console-2.5.4.jar:$APP_HOME/lib/groovy-groovydoc-2.5.4.jar:$APP_HOME/lib/groovy-docgenerator-2.5.4.jar:$APP_HOME/lib/groovy-cli-picocli-2.5.4.jar:$APP_HOME/lib/groovy-datetime-2.5.4.jar:$APP_HOME/lib/groovy-jmx-2.5.4.jar:$APP_HOME/lib/groovy-json-2.5.4.jar:$APP_HOME/lib/groovy-jsr223-2.5.4.jar:$APP_HOME/lib/groovy-nio-2.5.4.jar:$APP_HOME/lib/groovy-servlet-2.5.4.jar:$APP_HOME/lib/groovy-sql-2.5.4.jar:$APP_HOME/lib/groovy-swing-2.5.4.jar:$APP_HOME/lib/groovy-test-2.5.4.jar:$APP_HOME/lib/groovy-test-junit5-2.5.4.jar:$APP_HOME/lib/groovy-testng-2.5.4.jar:$APP_HOME/lib/geb-ast-4.1.jar:$APP_HOME/lib/geb-waiting-4.1.jar:$APP_HOME/lib/geb-implicit-assertions-4.1.jar:$APP_HOME/lib/geb-exceptions-4.1.jar:$APP_HOME/lib/groovy-templates-2.5.14.jar:$APP_HOME/lib/groovy-xml-2.5.14.jar:$APP_HOME/lib/groovy-macro-2.5.14.jar:$APP_HOME/lib/groovy-2.5.14.jar:$APP_HOME/lib/multiverse-core-0.7.0.jar:$APP_HOME/lib/jsr166y-1.7.0.jar:$APP_HOME/lib/jodd-lagarto-5.1.5.jar:$APP_HOME/lib/jodd-log-5.1.5.jar:$APP_HOME/lib/zt-exec-1.7.jar:$APP_HOME/lib/slf4j-api-1.7.30.jar:$APP_HOME/lib/httpmime-4.5.13.jar:$APP_HOME/lib/httpclient-cache-4.5.10.jar:$APP_HOME/lib/httpclient-4.5.13.jar:$APP_HOME/lib/httpcore-nio-4.4.13.jar:$APP_HOME/lib/httpasyncclient-4.1.4.jar:$APP_HOME/lib/gson-2.8.6.jar:$APP_HOME/lib/selenium-api-4.0.0-alpha-2.jar:$APP_HOME/lib/threeten-extra-1.4.jar:$APP_HOME/lib/byte-buddy-1.10.14.jar:$APP_HOME/lib/commons-exec-1.3.jar:$APP_HOME/lib/okhttp-3.14.0.jar:$APP_HOME/lib/okio-1.17.2.jar:$APP_HOME/lib/mongodb-driver-core-3.4.3.jar:$APP_HOME/lib/bson-3.4.3.jar:$APP_HOME/lib/shiro-core-1.2.4.jar:$APP_HOME/lib/snakeyaml-1.27.jar:$APP_HOME/lib/netty-resolver-dns-4.1.52.Final.jar:$APP_HOME/lib/netty-codec-http2-4.1.8.Final.jar:$APP_HOME/lib/netty-handler-4.1.52.Final.jar:$APP_HOME/lib/netty-handler-proxy-4.1.8.Final.jar:$APP_HOME/lib/netty-codec-http-4.1.8.Final.jar:$APP_HOME/lib/netty-codec-dns-4.1.52.Final.jar:$APP_HOME/lib/netty-codec-socks-4.1.8.Final.jar:$APP_HOME/lib/netty-codec-4.1.52.Final.jar:$APP_HOME/lib/netty-transport-4.1.52.Final.jar:$APP_HOME/lib/netty-buffer-4.1.52.Final.jar:$APP_HOME/lib/netty-resolver-4.1.52.Final.jar:$APP_HOME/lib/netty-common-4.1.52.Final.jar:$APP_HOME/lib/cache-api-1.0.0.jar:$APP_HOME/lib/reactor-core-3.3.9.RELEASE.jar:$APP_HOME/lib/rxjava-2.2.19.jar:$APP_HOME/lib/jboss-marshalling-river-2.0.10.Final.jar:$APP_HOME/lib/jodd-bean-5.1.6.jar:$APP_HOME/lib/failureaccess-1.0.1.jar:$APP_HOME/lib/listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar:$APP_HOME/lib/jsr305-3.0.2.jar:$APP_HOME/lib/checker-qual-3.8.0.jar:$APP_HOME/lib/error_prone_annotations-2.5.1.jar:$APP_HOME/lib/j2objc-annotations-1.3.jar:$APP_HOME/lib/selenium-server-4.0.0-alpha-2.jar:$APP_HOME/lib/classgraph-4.8.47.jar:$APP_HOME/lib/commons-lang-2.6.jar:$APP_HOME/lib/ant-junit-1.9.13.jar:$APP_HOME/lib/ant-1.9.13.jar:$APP_HOME/lib/ant-launcher-1.9.13.jar:$APP_HOME/lib/ant-antlr-1.9.13.jar:$APP_HOME/lib/commons-cli-1.4.jar:$APP_HOME/lib/picocli-3.7.0.jar:$APP_HOME/lib/qdox-1.12.1.jar:$APP_HOME/lib/jline-2.14.6.jar:$APP_HOME/lib/junit-4.12.jar:$APP_HOME/lib/junit-platform-launcher-1.3.1.jar:$APP_HOME/lib/junit-jupiter-engine-5.3.1.jar:$APP_HOME/lib/testng-6.13.1.jar:$APP_HOME/lib/httpcore-4.4.13.jar:$APP_HOME/lib/commons-logging-1.2.jar:$APP_HOME/lib/commons-codec-1.11.jar:$APP_HOME/lib/jodd-core-5.1.6.jar:$APP_HOME/lib/commons-beanutils-1.8.3.jar:$APP_HOME/lib/reactive-streams-1.0.3.jar:$APP_HOME/lib/jboss-marshalling-2.0.10.Final.jar:$APP_HOME/lib/opentracing-tracerresolver-0.1.7.jar:$APP_HOME/lib/opentracing-util-0.33.0.jar:$APP_HOME/lib/jna-4.2.2.jar:$APP_HOME/lib/commons-io-2.2.jar:$APP_HOME/lib/hamcrest-core-1.3.jar:$APP_HOME/lib/junit-platform-engine-1.3.1.jar:$APP_HOME/lib/junit-jupiter-api-5.3.1.jar:$APP_HOME/lib/junit-platform-commons-1.3.1.jar:$APP_HOME/lib/apiguardian-api-1.0.0.jar:$APP_HOME/lib/jcommander-1.72.jar:$APP_HOME/lib/log4j-api-2.8.2.jar:$APP_HOME/lib/opentracing-noop-0.33.0.jar:$APP_HOME/lib/opentracing-api-0.33.0.jar:$APP_HOME/lib/opentest4j-1.1.1.jar


# Determine the Java command to use to start the JVM.
if [ -n "$JAVA_HOME" ] ; then
    if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
        # IBM's JDK on AIX uses strange locations for the executables
        JAVACMD="$JAVA_HOME/jre/sh/java"
    else
        JAVACMD="$JAVA_HOME/bin/java"
    fi
    if [ ! -x "$JAVACMD" ] ; then
        die "ERROR: JAVA_HOME is set to an invalid directory: $JAVA_HOME

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
    fi
else
    JAVACMD="java"
    which java >/dev/null 2>&1 || die "ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.

Please set the JAVA_HOME variable in your environment to match the
location of your Java installation."
fi

# Increase the maximum file descriptors if we can.
if [ "$cygwin" = "false" -a "$darwin" = "false" -a "$nonstop" = "false" ] ; then
    MAX_FD_LIMIT=`ulimit -H -n`
    if [ $? -eq 0 ] ; then
        if [ "$MAX_FD" = "maximum" -o "$MAX_FD" = "max" ] ; then
            MAX_FD="$MAX_FD_LIMIT"
        fi
        ulimit -n $MAX_FD
        if [ $? -ne 0 ] ; then
            warn "Could not set maximum file descriptor limit: $MAX_FD"
        fi
    else
        warn "Could not query maximum file descriptor limit: $MAX_FD_LIMIT"
    fi
fi

# For Darwin, add options to specify how the application appears in the dock
if $darwin; then
    GRADLE_OPTS="$GRADLE_OPTS \"-Xdock:name=$APP_NAME\" \"-Xdock:icon=$APP_HOME/media/gradle.icns\""
fi

# For Cygwin or MSYS, switch paths to Windows format before running java
if [ "$cygwin" = "true" -o "$msys" = "true" ] ; then
    APP_HOME=`cygpath --path --mixed "$APP_HOME"`
    CLASSPATH=`cygpath --path --mixed "$CLASSPATH"`

    JAVACMD=`cygpath --unix "$JAVACMD"`

    # We build the pattern for arguments to be converted via cygpath
    ROOTDIRSRAW=`find -L / -maxdepth 1 -mindepth 1 -type d 2>/dev/null`
    SEP=""
    for dir in $ROOTDIRSRAW ; do
        ROOTDIRS="$ROOTDIRS$SEP$dir"
        SEP="|"
    done
    OURCYGPATTERN="(^($ROOTDIRS))"
    # Add a user-defined pattern to the cygpath arguments
    if [ "$GRADLE_CYGPATTERN" != "" ] ; then
        OURCYGPATTERN="$OURCYGPATTERN|($GRADLE_CYGPATTERN)"
    fi
    # Now convert the arguments - kludge to limit ourselves to /bin/sh
    i=0
    for arg in "$@" ; do
        CHECK=`echo "$arg"|egrep -c "$OURCYGPATTERN" -`
        CHECK2=`echo "$arg"|egrep -c "^-"`                                 ### Determine if an option

        if [ $CHECK -ne 0 ] && [ $CHECK2 -eq 0 ] ; then                    ### Added a condition
            eval `echo args$i`=`cygpath --path --ignore --mixed "$arg"`
        else
            eval `echo args$i`="\"$arg\""
        fi
        i=`expr $i + 1`
    done
    case $i in
        0) set -- ;;
        1) set -- "$args0" ;;
        2) set -- "$args0" "$args1" ;;
        3) set -- "$args0" "$args1" "$args2" ;;
        4) set -- "$args0" "$args1" "$args2" "$args3" ;;
        5) set -- "$args0" "$args1" "$args2" "$args3" "$args4" ;;
        6) set -- "$args0" "$args1" "$args2" "$args3" "$args4" "$args5" ;;
        7) set -- "$args0" "$args1" "$args2" "$args3" "$args4" "$args5" "$args6" ;;
        8) set -- "$args0" "$args1" "$args2" "$args3" "$args4" "$args5" "$args6" "$args7" ;;
        9) set -- "$args0" "$args1" "$args2" "$args3" "$args4" "$args5" "$args6" "$args7" "$args8" ;;
    esac
fi

# Escape application args
save () {
    for i do printf %s\\n "$i" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/' \\\\/" ; done
    echo " "
}
APP_ARGS=`save "$@"`

# Collect all arguments for the java command, following the shell quoting and substitution rules
eval set -- $DEFAULT_JVM_OPTS $JAVA_OPTS $MASOFFER_TRAFFIC_BUFF_OPTS -classpath "\"$CLASSPATH\"" app.Runner "$APP_ARGS"

exec "$JAVACMD" "$@"
