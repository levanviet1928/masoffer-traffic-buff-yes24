package io.actions

import com.machinepublishers.jbrowserdriver.UserAgent
import groovy.util.logging.Slf4j
import model.Action
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.remote.RemoteWebDriver

import java.util.concurrent.TimeUnit

@Slf4j
class ActionClick implements IActionLoader {

    @Override
    void loadActions(Action actionModel, RemoteWebDriver driver) {
        String xPath = actionModel.source
        def foundElement = driver.findElementsByXPath(xPath)
        if (foundElement.isEmpty()) {
            log.error("${xPath} - element not found")
            return
        }
        WebElement foundElement1 = driver.findElementByXPath(xPath)
        new Actions(driver).moveToElement(foundElement1).click().perform()

        sleep(TimeUnit.MILLISECONDS.convert(actionModel.delay, TimeUnit.SECONDS))
    }
}
