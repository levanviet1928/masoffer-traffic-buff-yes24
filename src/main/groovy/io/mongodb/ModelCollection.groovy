package io.mongodb

import com.mongodb.client.model.*
import org.bson.Document
import org.bson.conversions.Bson
import org.bson.types.ObjectId
import mapper.ModelMapper

import java.util.concurrent.CompletableFuture

/**
 * For MongoDB Async Driver
 * Created by chipn@eway.vn on 2/7/17.
 */

class ModelCollection<T> extends DocumentCollection {

    final ModelMapper<T> mapper

    ModelCollection(String uri, Class<? extends ModelMapper<T>> mapperClass) {
        this(uri, mapperClass.newInstance())
    }

    ModelCollection(String uri, ModelMapper<T> mapper) {
        super(uri)
        this.mapper = mapper
    }

    CompletableFuture<T> getModel(String id) {
        return findModel(id).first()
    }

//    CompletableFuture<List<T>> getModels(List<ObjectId> ids) {
//        return findModels(ids).intoModels()
//    }

    DocumentFindIterable find(String id) {
        return findDocument([_id: new ObjectId(id)] as Document)
    }

    ModelFindIterable<T> findModel(String id) {
        return new ModelFindIterable<T>(find(id), mapper)
    }

    ModelFindIterable<T> findModels() {
        return new ModelFindIterable(findDocument(), mapper)
    }

    ModelFindIterable<T> findModels(Bson filter) {
        return new ModelFindIterable(findDocument(filter), mapper)
    }

    CompletableFuture<T> findOneAndUpdateModel(ObjectId id, Bson update) {
        return findOneAndUpdate(id, update).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> findOneAndUpdateModel(ObjectId id, Bson update, FindOneAndUpdateOptions options) {
        return findOneAndUpdate(id, update, options).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> findOneAndUpdateModel(Bson filter, Bson update) {
        return findOneAndUpdate(filter, update).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> findOneAndUpdateModel(Bson filter, Bson update, FindOneAndUpdateOptions options) {
        return findOneAndUpdate(filter, update, options).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> findOneAndReplaceModel(ObjectId id, Document replacement) {
        return findOneAndReplace(id, replacement).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> findOneAndReplaceModel(ObjectId id, Document replacement, FindOneAndReplaceOptions options) {
        return findOneAndReplace(id, replacement, options).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> findOneAndReplaceModel(Bson filter, Document replacement) {
        return findOneAndReplace(filter, replacement).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> findOneAndReplaceModel(Bson filter, Document replacement, FindOneAndReplaceOptions options) {
        return findOneAndReplace(filter, replacement, options).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> findOneAndDeleteModel(ObjectId id) {
        return findOneAndDelete(id).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> findOneAndDeleteModel(ObjectId id, FindOneAndDeleteOptions options) {
        return findOneAndDelete(id, options).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> findOneAndDeleteModel(Bson filter) {
        return findOneAndDelete(filter).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> findOneAndDeleteModel(Bson filter, FindOneAndDeleteOptions options) {
        return findOneAndDelete(filter, options).thenApply { mapper.toModel(it) }
    }

    CompletableFuture<T> insertOneModel(T model) {
        def document = mapper.toDocument(model)
        return insertOne(document).thenApply { it ->
           return mapper.toModel(it)
        }
    }

    CompletableFuture<T> insertOneModel(T model, InsertOneOptions options) {
        def document = mapper.toDocument(model)
        return insertOne(document).thenApply { mapper.toModel(document) }
    }

    CompletableFuture<List<T>> insertManyModels(List<T> models) {
        def documents = mapper.toDocuments(models)
        return insertMany(documents).thenApply { mapper.toModels(documents) }
    }

    CompletableFuture<List<T>> insertManyModels(List<T> models, InsertManyOptions options) {
        def documents = mapper.toDocuments(models)
        return insertMany(documents, options).thenApply { mapper.toModels(documents) }
    }

}

