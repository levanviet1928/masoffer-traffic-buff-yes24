package io.mongodb

import com.mongodb.async.AsyncBatchCursor
import org.bson.Document
import mapper.ModelMapper

import java.util.concurrent.CompletableFuture

/**
 * Created by chipn@eway.vn on 2/9/17.
 */
class ModelBatchCursor<T> {

    private final AsyncBatchCursor<Document> delegate
    private final ModelMapper<T> mapper

    ModelBatchCursor(AsyncBatchCursor<Document> delegate, ModelMapper<T> mapper) {
        this.delegate = delegate
        this.mapper = mapper
    }

    CompletableFuture<List<Document>> next() {
        MongoCompletableFuture future = new MongoCompletableFuture<List<Document>>()
        delegate.next(future)
        return future
    }

    CompletableFuture<List<T>> nextModels() {
        return next().thenApply { documents -> mapper.toModels(documents) }
    }

    void setBatchSize(int batchSize) {
        delegate.setBatchSize(batchSize)
    }

    int getBatchSize() {
        return delegate.getBatchSize()
    }

    boolean isClosed() {
        return delegate.isClosed()
    }

    void close() {
        delegate.close()
    }

}
