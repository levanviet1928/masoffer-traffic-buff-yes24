package io.mongodb

import com.mongodb.async.AsyncBatchCursor
import org.bson.Document

import java.util.concurrent.CompletableFuture

/**
 * Created by chipn@eway.vn on 2/9/17.
 */
class DocumentBatchCursor {

    private final AsyncBatchCursor<Document> delegate

    DocumentBatchCursor(AsyncBatchCursor<Document> delegate) {
        this.delegate = delegate
    }

    CompletableFuture<List<Document>> next() {
        MongoCompletableFuture<List<Document>> future = new MongoCompletableFuture<>()
        delegate.next(future)
        return future
    }

    void setBatchSize(int batchSize) {
        delegate.setBatchSize(batchSize)
    }

    int getBatchSize() {
        return delegate.getBatchSize()
    }

    boolean isClosed() {
        return delegate.isClosed()
    }

    void close() {
        delegate.close()
    }
}